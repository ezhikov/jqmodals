###*
# @preserve
# @file
# @version 2.1.0
# @author Kirill Popolov (https://github.com/ezhikov)
# @licence MIT

###
"use strict";
###*
# @class Modal
# @final
# @prop {jQuery} Modal#popups All popups
# @prop {jQuery[]} Modal#parentPopup Stack of opened popups for nested open
# @param {jQuery} elem Wrapper for all popups
# @param {string} [klass=".popup"] Items class
###
class Modal
  constructor: (elem, klass = ".popup")->
    $.extend(@, $(elem))
    @css
      height: '100%'
      width: '100%'
      display : 'none'
      position: 'fixed'
      top     : 0
      left    : 0
    @popups = @.find(klass)
    @parentPopup = []
###*
# Adds popup into list of available popups
# @function Modal#addPopup
# @param {jQuery|string|HTMLElement} popup Popup which you intend to add into popups list
# @return {Modal}
###
Modal::addPopup = (popup) ->
  @popups = @popups.add($(popup).appendTo @)
  @
###*
# Opens popup.
# @function Modal#open
# @param {jQuery|string|HTMLElement} target Target popup
# @param {function|Deferred|jqXHR} [ajax] jqXHR or Deferred or function. Waits for resolve before close popup.
# @return {jQuery} Target popup
###
Modal::open = (target, ajax) ->
  $target = @popups.filter target
  return $target if $target.hasClass "active"
  @popups.hide()
  @parentPopup.push @popups.filter(".active").removeClass("active") if @popups.filter(".active").length
  opening = ->
    @add(target).show()
    $target.addClass "active"
    return
  if ajax && typeof ajax == 'function'
    $.when(ajax(target)).done ->
      opening.call(this)
      return
  else
    opening.call(this)
  $("body").css overflow: "hidden"
  $target
###*
# Close current popup and (if exists) opens parent popup.
# @function Modal#close
# @param {function|Deferred|jqXHR} [ajax] jqXHR or Deferred or function. Waits for resolve before close popup.
# @return {Modal}
###
Modal::close = (ajax) ->
  return false  unless @popups.filter(".active").length
  $target = $(@popups.filter(".active"))
  @popups.hide().removeClass "active"
  if ajax && typeof ajax is 'function'
    $.when(ajax).then @hide()
  else
    @hide()
  par = @parentPopup.pop()
  $("body").css overflow: "auto"
  $target.trigger "closepopup"
  @open par  if par
  return @
###*
# Close all popups
# @function Modal#closeAll
# @param {function|Deferred|jqXHR} [ajax] jqXHR or Deferred or function. Waits for resolve before close popup.
# @return {Modal}
###
Modal::closeAll = (ajax)->
  @parentPopup = []
  return @close ajax


(($)->
  ###*
  # jQuery-wrapper for {@link Modal}.
  # @function jQuery.fn#jQmodals
  # @alias jQuery.jQmodals
  # @namespace Modal
  # @global
  # @param {string} [klass=".popup"] Items class
  # @example
  # modal = $(elem).jQmodals()
  # modal.open(target)
  ###
  $.fn.jQmodals = (klass)->
    modal = new Modal(this, klass)
    modal
  return) jQuery




