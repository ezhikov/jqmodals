# jQmodals

jQuery plugin for modal popups

v 1.1.0 - ".popup" class now customizable

v 2.0.1 - "closeAll" method, new docs, some bugfixes

v 2.1.0 - wrapper styles automated

v 2.1.1 - css bugfix

* [Usage](#usage)
* [Api](#api)
* [TODO](#todo)


## Usage

Download plugin or install via npm (``npm install jqmodals``)

* Insert your popups into wrapper, like this:

```html
<div class="wrapper">
    <div><!-- content --></div>
    <div><!-- content --></div>
    <div><!-- content --></div>
</div>
```

* Add ``.popup`` class to your popups

* initialize it:

```js
$('.wrapper').jQmodals();
```

* Set your triggers

```js
$('.open').on('mousedown', function(){
    modal.open.('#first');
)}
```

## API

<a name="Modal"></a>
#Modal
##jQuery.fn.jQmodals
jQuery-wrapper for [Modal](#Modal).

**Params**

- \[klass=".popup"\] `string` - Items class

**Example**
modal = $(elem).jQmodals()
modal.open(target)

**Members**

* [Modal](#Modal)
  * [modal.addPopup(popup)](#Modal#addPopup)
  * [modal.open(target, [ajax])](#Modal#open)
  * [modal.close([ajax])](#Modal#close)
  * [modal.closeAll([ajax])](#Modal#closeAll)

<a name="Modal#addPopup"></a>
##modal.addPopup(popup)
Adds popup into list of available popups

**Params**

- popup `jQuery` | `string` | `HTMLElement` - Popup which you intend to add into popups list

**Returns**: [Modal](#Modal)
<a name="Modal#open"></a>
##modal.open(target, [ajax])
Opens popup.

**Params**

- target `jQuery` | `string` | `HTMLElement` - Target popup
- \[ajax\] `function` | `Deferred` | `jqXHR` - jqXHR or Deferred or function. Waits for resolve before close popup.

**Returns**: `jQuery` - Target popup
<a name="Modal#close"></a>
##modal.close([ajax])
Close current popup and (if exists) opens parent popup.

**Params**

- \[ajax\] `function` | `Deferred` | `jqXHR` - jqXHR or Deferred or function. Waits for resolve before close popup.

**Returns**: [Modal](#Modal)
<a name="Modal#closeAll"></a>
##modal.closeAll([ajax])
Close all popups

**Params**

- \[ajax\] `function` | `Deferred` | `jqXHR` - jqXHR or Deferred or function. Waits for resolve before close popup.

**Returns**: [Modal](#Modal)


## Events
###closepopup
Fires after popup closed with current popup as target

``this`` references to targeted popup


##TODO
- [x] ``.popup`` class optionsl
- [x] ``closeAll`` method
- [x] automate main wrapper styles
